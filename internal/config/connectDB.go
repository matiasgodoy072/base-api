package config

import (
	"fmt"
	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"os"
)

type Config struct {
	DBHost     string
	DBUser     string
	DBPassword string
	DBName     string
	DBPort     string
}

// Función para inicializar la base de datos
func InitDB() *gorm.DB {
	// Cargar variables de entorno
	err := godotenv.Load() // Asumiendo que estás usando "github.com/joho/godotenv"
	if err != nil {
		log.Fatalf("Error al cargar variables de entorno: %v", err)
	}

	// Obtener valores de las variables de entorno
	config := Config{
		DBHost:     os.Getenv("DB_HOST"),
		DBUser:     os.Getenv("DB_USER"),
		DBPassword: os.Getenv("DB_PASSWORD"),
		DBName:     os.Getenv("DB_NAME"),
		DBPort:     os.Getenv("DB_PORT"),
	}

	// Validar que las variables de entorno estén presentes
	if config.DBHost == "" || config.DBUser == "" || config.DBPassword == "" || config.DBName == "" || config.DBPort == "" {
		log.Fatalf("Faltan variables de entorno necesarias para la configuración de la base de datos")
	}

	// Construir el DSN
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=America/Santiago",
		config.DBHost, config.DBUser, config.DBPassword, config.DBName, config.DBPort)

	// Conectar a la base de datos
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		SkipDefaultTransaction: true,
	})
	if err != nil {
		log.Fatalf("Error al conectar a la base de datos: %v", err)
	}

	return db
}
