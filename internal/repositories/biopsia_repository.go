package repositories

import (
	"go-biopsia/internal/models"
	"gorm.io/gorm"
)

type BiopsiaRepository interface {
	GetAllBiopsias(limit int, offset int) ([]*models.Biopsia, error)
	GetBiopsiaByID(id int) (*models.Biopsia, error)
	CreateBiopsia(biopsia *models.Biopsia) error
	UpdateBiopsia(id int, updatedBiopsia *models.Biopsia) (*models.Biopsia, error)
	GetBiopsiasByCondition(condition string, value interface{}) ([]*models.Biopsia, error)
	GetBiopsiasByDateRange(desde string, hasta string) ([]*models.Biopsia, error)
}

type biopsiaRepository struct {
	db *gorm.DB
}

func NewBiopsiaRepository(db *gorm.DB) BiopsiaRepository {
	return &biopsiaRepository{db}
}

func (b *biopsiaRepository) GetAllBiopsias(limit int, offset int) ([]*models.Biopsia, error) {
	var biopsias []*models.Biopsia
	err := b.db.Limit(limit).Offset(offset).Find(&biopsias).Error
	return biopsias, err
}

func (b *biopsiaRepository) GetBiopsiaByID(id int) (*models.Biopsia, error) {
	var biopsia models.Biopsia

	err := b.db.First(&biopsia, id).Error
	return &biopsia, err
}

func (b *biopsiaRepository) CreateBiopsia(biopsia *models.Biopsia) error {
	return b.db.Create(biopsia).Error
}

func (b *biopsiaRepository) UpdateBiopsia(id int, updatedBiopsia *models.Biopsia) (*models.Biopsia, error) {
	var biopsia *models.Biopsia
	if err := b.db.First(&biopsia, id).Error; err != nil {
		return biopsia, err
	}
	b.db.Model(&biopsia).Updates(updatedBiopsia)
	return biopsia, nil
}

func (b *biopsiaRepository) GetBiopsiasByCondition(condition string, value interface{}) ([]*models.Biopsia, error) {
	var biopsias []*models.Biopsia
	result := b.db.Where(condition, value).Find(&biopsias)
	return biopsias, result.Error
}

func (b *biopsiaRepository) GetBiopsiasByDateRange(desde string, hasta string) ([]*models.Biopsia, error) {

	var biopsias []*models.Biopsia
	result := b.db.Where("fecha_recepcion_muestra BETWEEN ? AND ?", desde, hasta).Find(&biopsias)
	return biopsias, result.Error
}
