package controllers

import (
	"github.com/gin-gonic/gin"
	"go-biopsia/internal/models"
	"go-biopsia/internal/repositories"
	"net/http"
	"strconv"
)

type BiopsiaController struct {
	biopsaRepo repositories.BiopsiaRepository
}

func NewBiopsiaController(biopsaRepo repositories.BiopsiaRepository) *BiopsiaController {
	return &BiopsiaController{biopsaRepo}
}

// GetAllBiopsias godoc
// @Summary Get all biopsias with pagination
// @Description Get biopsias in chunks to handle large volumes
// @Tags biopsias
// @Accept  json
// @Produce  json
// @Param limit query int false "Limit"
// @Param offset query int false "Offset"
// @Success 200 {array} models.BiopsiaSwagger
// @Failure 500 {object} models.ErrorResponse
// @Router /biopsias [get]
func (bc *BiopsiaController) GetAllBiopsias(c *gin.Context) {
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "5000"))
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ErrorResponse{Message: "Invalid limit"})
		return
	}
	offset, err := strconv.Atoi(c.DefaultQuery("offset", "0"))
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ErrorResponse{Message: "Invalid offset"})
		return
	}

	biopsias, err := bc.biopsaRepo.GetAllBiopsias(limit, offset)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{Message: err.Error()})
		return
	}
	c.JSON(http.StatusOK, biopsias)
}

// GetBiopsiaByID godoc
// @Summary Get a biopsia by ID
// @Description Get a biopsia by its ID
// @Tags biopsias
// @Accept  json
// @Produce  json
// @Param id path int true "Biopsia ID"
// @Success 200 {object} models.BiopsiaSwagger
// @Failure 400 {object} models.ErrorResponse
// @Failure 404 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /biopsias/{id} [get]
func (bc *BiopsiaController) GetBiopsiaByID(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ErrorResponse{Message: "Invalid ID"})
		return
	}

	biopsia, err := bc.biopsaRepo.GetBiopsiaByID(id)
	if err != nil {
		if err.Error() == "record not found" {
			c.JSON(http.StatusNotFound, models.ErrorResponse{Message: "Biopsia not found"})
			return
		}
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{Message: err.Error()})
		return
	}
	c.JSON(http.StatusOK, biopsia)
}

// CreateBiopsia godoc
// @Summary Create a new biopsia
// @Description Create a new biopsia with the input payload
// @Tags biopsias
// @Accept  json
// @Produce  json
// @Param biopsia body models.BiopsiaSwagger true "Biopsia to create"
// @Success 201 {object} models.BiopsiaSwagger
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /biopsias [post]
func (bc *BiopsiaController) CreateBiopsia(c *gin.Context) {
	var biopsia models.Biopsia
	if err := c.ShouldBindJSON(&biopsia); err != nil {
		c.JSON(http.StatusBadRequest, models.ErrorResponse{Message: err.Error()})
		return
	}

	if err := bc.biopsaRepo.CreateBiopsia(&biopsia); err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{Message: err.Error()})
		return
	}

	c.JSON(http.StatusCreated, biopsia)
}

// UpdateBiopsia godoc
// @Summary Update a biopsia
// @Description Update a biopsia by its ID
// @Tags biopsias
// @Accept  json
// @Produce  json
// @Param id path int true "Biopsia ID"
// @Param biopsia body models.Biopsia true "Biopsia to update"
// @Success 200 {object} models.BiopsiaSwagger
// @Failure 400 {object} models.ErrorResponse
// @Failure 404 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /biopsias/{id} [patch]
func (bc *BiopsiaController) UpdateBiopsia(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ErrorResponse{Message: "Invalid ID"})
		return
	}

	var updatedBiopsia models.Biopsia
	if err := c.ShouldBindJSON(&updatedBiopsia); err != nil {
		c.JSON(http.StatusBadRequest, models.ErrorResponse{Message: err.Error()})
		return
	}

	biopsia, err := bc.biopsaRepo.UpdateBiopsia(id, &updatedBiopsia)
	if err != nil {
		if err.Error() == "record not found" {
			c.JSON(http.StatusNotFound, models.ErrorResponse{Message: "Biopsia not found"})
			return
		}
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{Message: err.Error()})
		return
	}
	c.JSON(http.StatusOK, biopsia)
}

// GetBiopsiasByCondition godoc
// @Summary Get biopsias by a specific condition
// @Description Get biopsias by a specific condition
// @Tags biopsias
// @Accept  json
// @Produce  json
// @Param condition query string true "Condition"
// @Param value query string true "Value"
// @Success 200 {array} models.BiopsiaSwagger
// @Failure 400 {object} models.ErrorResponse
// @Failure 404 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /biopsias/search [get]
func (bc *BiopsiaController) GetBiopsiasByCondition(c *gin.Context) {
	condition := c.Query("condition")
	value := c.Query("value")

	if condition == "" || value == "" {
		c.JSON(http.StatusBadRequest, models.ErrorResponse{Message: "Invalid query parameters"})
		return
	}

	biopsias, err := bc.biopsaRepo.GetBiopsiasByCondition(condition, value)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{Message: err.Error()})
		return
	}
	if len(biopsias) == 0 {
		c.JSON(http.StatusNotFound, models.ErrorResponse{Message: "Biopsias not found"})
		return
	}
	c.JSON(http.StatusOK, biopsias)
}

// GetBiopsiasByDateRange godoc
// @Summary Get biopsias by date range
// @Description Get biopsias by date range
// @Tags biopsias
// @Accept  json
// @Produce  json
// @Param desde query string true "Fecha de inicio (YYYY-MM-DD)"
// @Param hasta query string true "Fecha de fin (YYYY-MM-DD)"
// @Success 200 {array} models.BiopsiaSwagger
// @Failure 400 {object} models.ErrorResponse
// @Failure 404 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /biopsias/date-range [get]
func (bc *BiopsiaController) GetBiopsiasByDateRange(c *gin.Context) {
	desde := c.Query("desde")
	hasta := c.Query("hasta")

	if desde == "" || hasta == "" {
		c.JSON(http.StatusBadRequest, models.ErrorResponse{Message: "Invalid query parameters"})
		return
	}

	biopsias, err := bc.biopsaRepo.GetBiopsiasByDateRange(desde, hasta)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{Message: err.Error()})
		return
	}
	if len(biopsias) == 0 {
		c.JSON(http.StatusNotFound, models.ErrorResponse{Message: "Biopsias not found"})
		return
	}
	c.JSON(http.StatusOK, biopsias)
}

// PerformanceTestBiopsia godoc
// @Summary Insert multiple biopsias for performance testing
// @Description Insert multiple biopsias with the same data for performance testing
// @Tags biopsias
// @Accept  json
// @Produce  json
// @Param biopsia body models.BiopsiaSwagger true "Biopsia to create"
// @Param count query int true "Number of times to insert the biopsia"
// @Success 201 {object} models.BiopsiaSwagger
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /biopsias/performance [post]
func (bc *BiopsiaController) PerformanceTestBiopsia(c *gin.Context) {
	var biopsia models.Biopsia
	if err := c.ShouldBindJSON(&biopsia); err != nil {
		c.JSON(http.StatusBadRequest, models.ErrorResponse{Message: err.Error()})
		return
	}

	count, err := strconv.Atoi(c.DefaultQuery("count", "1"))
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ErrorResponse{Message: "Invalid count"})
		return
	}

	for i := 0; i < count; i++ {
		if err := bc.biopsaRepo.CreateBiopsia(&biopsia); err != nil {
			c.JSON(http.StatusInternalServerError, models.ErrorResponse{Message: err.Error()})
			return
		}
	}
	c.JSON(http.StatusCreated, biopsia)
}
