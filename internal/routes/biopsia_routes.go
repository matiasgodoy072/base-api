package routes

import (
	"github.com/gin-gonic/gin"
	"go-biopsia/internal/controllers"
	"go-biopsia/internal/repositories"
)

func BiopsiaRoutes(router *gin.Engine, biopsiaRepo repositories.BiopsiaRepository) {

	biopsiaController := controllers.NewBiopsiaController(biopsiaRepo)

	biopsias := router.Group("/biopsias")
	{
		biopsias.GET("/", biopsiaController.GetAllBiopsias)
		biopsias.GET("/:id", biopsiaController.GetBiopsiaByID)
		biopsias.GET("/search", biopsiaController.GetBiopsiasByCondition)
		biopsias.GET("/date-range", biopsiaController.GetBiopsiasByDateRange)
		biopsias.POST("/", biopsiaController.CreateBiopsia)
		biopsias.PATCH("/:id", biopsiaController.UpdateBiopsia)
		biopsias.POST("/performance", biopsiaController.PerformanceTestBiopsia)
	}
}
