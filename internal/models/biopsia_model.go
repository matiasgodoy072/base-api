package models

import "time"

type Biopsia struct {
	ID                    int        `gorm:"primaryKey;column:id"`
	NroBiopsia            string     `gorm:"size:10;not null;column:nro_biopsia"`
	MedicoTratante        *int       `gorm:"index;column:medico_tratante"`
	FechaTomaMuestra      *time.Time `gorm:"column:fecha_toma_muestra"`
	FechaRecepcionMuestra *time.Time `gorm:"column:fecha_recepcion_muestra"`
	Servicio              *int       `gorm:"column:servicio"`
	Organo1               *int       `gorm:"column:organo1"`
	Organo2               *int       `gorm:"column:organo2"`
	Patologo              *int       `gorm:"column:patologo"`
	Tecnologo             *int       `gorm:"column:tecnologo"`
	Dias                  *int       `gorm:"column:dias"`
	FechaInforme          *time.Time `gorm:"column:fecha_informe"`
	FechaEntregaInforme   *time.Time `gorm:"column:fecha_entrega_informe"`
	Biopsia               *int       `gorm:"column:biopsia"`
	BiopsiaRapida         *int       `gorm:"column:biopsia_rapida"`
	Citologico            *int       `gorm:"column:citologico"`
	Necropsia             *int       `gorm:"column:necropsia"`
	Placas                *int       `gorm:"column:placas"`
	MuestraEnviada        *string    `gorm:"size:255;column:muestra_enviada"`
	Antecedentes          *string    `gorm:"size:255;column:antecedentes"`
	IdTipoInforme         *int       `gorm:"column:id_tipo_informe"`
	IdFormatoMuestra      *int       `gorm:"index;column:id_formato_muestra"`
	PersonaId             int        `gorm:"not null;column:persona_id;default:null"`
	Auge                  *bool      `gorm:"column:auge"`
	PrevisionId           *int       `gorm:"column:prevision_id"`
	CantidadMuestra       *int       `gorm:"column:cantidad_muestra"`
	InformeProcesado      *bool      `gorm:"column:informe_procesado"`
	IdDt                  *int       `gorm:"column:id_dt"`
	CentroCostoId         *int       `gorm:"column:centro_costo_id"`
}
