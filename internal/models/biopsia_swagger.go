package models

import "time"

// BiopsiaSwagger represents the response structure for Swagger documentation
type BiopsiaSwagger struct {
	NroBiopsia            string     `binding:"required" example:"B123456"`
	MedicoTratante        *int       `example:"123"`
	FechaTomaMuestra      *time.Time `example:"2023-05-21T00:00:00Z"`
	FechaRecepcionMuestra *time.Time `example:"2023-05-22T00:00:00Z"`
	Servicio              *int       `example:"2"`
	Organo1               *int       `example:"1"`
	Organo2               *int       `example:"2"`
	Patologo              *int       `example:"456"`
	Tecnologo             *int       `example:"789"`
	Dias                  *int       `example:"3"`
	FechaInforme          *time.Time `example:"2023-05-25T00:00:00Z"`
	FechaEntregaInforme   *time.Time `example:"2023-05-26T00:00:00Z"`
	Biopsia               *int       `example:"1"`
	BiopsiaRapida         *int       `example:"0"`
	Citologico            *int       `example:"0"`
	Necropsia             *int       `example:"0"`
	Placas                *int       `example:"5"`
	MuestraEnviada        *string    `example:"Tejido"`
	Antecedentes          *string    `example:"Historial de enfermedad"`
	IdTipoInforme         *int       `example:"1"`
	IdFormatoMuestra      *int       `example:"2"`
	PersonaId             int        `binding:"required" example:"321"`
	Auge                  *bool      `example:"true"`
	PrevisionId           *int       `example:"1"`
	CantidadMuestra       *int       `example:"2"`
	InformeProcesado      *bool      `example:"false"`
	IdDt                  *int       `example:"10"`
	CentroCostoId         *int       `example:"20"`
}
