package models

// ErrorResponse represents the error response structure
type ErrorResponse struct {
	Message string `json:"message"`
}
