definitions:
  models.Biopsia:
    properties:
      antecedentes:
        type: string
      auge:
        type: boolean
      biopsia:
        type: integer
      biopsiaRapida:
        type: integer
      cantidadMuestra:
        type: integer
      centroCostoId:
        type: integer
      citologico:
        type: integer
      dias:
        type: integer
      fechaEntregaInforme:
        type: string
      fechaInforme:
        type: string
      fechaRecepcionMuestra:
        type: string
      fechaTomaMuestra:
        type: string
      id:
        type: integer
      idDt:
        type: integer
      idFormatoMuestra:
        type: integer
      idTipoInforme:
        type: integer
      informeProcesado:
        type: boolean
      medicoTratante:
        type: integer
      muestraEnviada:
        type: string
      necropsia:
        type: integer
      nroBiopsia:
        type: string
      organo1:
        type: integer
      organo2:
        type: integer
      patologo:
        type: integer
      personaId:
        type: integer
      placas:
        type: integer
      previsionId:
        type: integer
      servicio:
        type: integer
      tecnologo:
        type: integer
    type: object
  models.BiopsiaSwagger:
    properties:
      antecedentes:
        example: Historial de enfermedad
        type: string
      auge:
        example: true
        type: boolean
      biopsia:
        example: 1
        type: integer
      biopsiaRapida:
        example: 0
        type: integer
      cantidadMuestra:
        example: 2
        type: integer
      centroCostoId:
        example: 20
        type: integer
      citologico:
        example: 0
        type: integer
      dias:
        example: 3
        type: integer
      fechaEntregaInforme:
        example: "2023-05-26T00:00:00Z"
        type: string
      fechaInforme:
        example: "2023-05-25T00:00:00Z"
        type: string
      fechaRecepcionMuestra:
        example: "2023-05-22T00:00:00Z"
        type: string
      fechaTomaMuestra:
        example: "2023-05-21T00:00:00Z"
        type: string
      idDt:
        example: 10
        type: integer
      idFormatoMuestra:
        example: 2
        type: integer
      idTipoInforme:
        example: 1
        type: integer
      informeProcesado:
        example: false
        type: boolean
      medicoTratante:
        example: 123
        type: integer
      muestraEnviada:
        example: Tejido
        type: string
      necropsia:
        example: 0
        type: integer
      nroBiopsia:
        example: B123456
        type: string
      organo1:
        example: 1
        type: integer
      organo2:
        example: 2
        type: integer
      patologo:
        example: 456
        type: integer
      personaId:
        example: 321
        type: integer
      placas:
        example: 5
        type: integer
      previsionId:
        example: 1
        type: integer
      servicio:
        example: 2
        type: integer
      tecnologo:
        example: 789
        type: integer
    required:
    - nroBiopsia
    - personaId
    type: object
  models.ErrorResponse:
    properties:
      message:
        type: string
    type: object
info:
  contact:
    email: support@swagger.io
    name: API Support
    url: http://www.swagger.io/support
  description: This is a sample server for a biopsia management application.
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  termsOfService: http://swagger.io/terms/
  title: My Project API
  version: "1.0"
paths:
  /biopsias:
    get:
      consumes:
      - application/json
      description: Get biopsias in chunks to handle large volumes
      parameters:
      - description: Limit
        in: query
        name: limit
        type: integer
      - description: Offset
        in: query
        name: offset
        type: integer
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/models.BiopsiaSwagger'
            type: array
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/models.ErrorResponse'
      summary: Get all biopsias with pagination
      tags:
      - biopsias
    post:
      consumes:
      - application/json
      description: Create a new biopsia with the input payload
      parameters:
      - description: Biopsia to create
        in: body
        name: biopsia
        required: true
        schema:
          $ref: '#/definitions/models.BiopsiaSwagger'
      produces:
      - application/json
      responses:
        "201":
          description: Created
          schema:
            $ref: '#/definitions/models.BiopsiaSwagger'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/models.ErrorResponse'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/models.ErrorResponse'
      summary: Create a new biopsia
      tags:
      - biopsias
  /biopsias/{id}:
    get:
      consumes:
      - application/json
      description: Get a biopsia by its ID
      parameters:
      - description: Biopsia ID
        in: path
        name: id
        required: true
        type: integer
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.BiopsiaSwagger'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/models.ErrorResponse'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/models.ErrorResponse'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/models.ErrorResponse'
      summary: Get a biopsia by ID
      tags:
      - biopsias
    patch:
      consumes:
      - application/json
      description: Update a biopsia by its ID
      parameters:
      - description: Biopsia ID
        in: path
        name: id
        required: true
        type: integer
      - description: Biopsia to update
        in: body
        name: biopsia
        required: true
        schema:
          $ref: '#/definitions/models.Biopsia'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/models.BiopsiaSwagger'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/models.ErrorResponse'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/models.ErrorResponse'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/models.ErrorResponse'
      summary: Update a biopsia
      tags:
      - biopsias
  /biopsias/date-range:
    get:
      consumes:
      - application/json
      description: Get biopsias by date range
      parameters:
      - description: Fecha de inicio (YYYY-MM-DD)
        in: query
        name: desde
        required: true
        type: string
      - description: Fecha de fin (YYYY-MM-DD)
        in: query
        name: hasta
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/models.BiopsiaSwagger'
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/models.ErrorResponse'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/models.ErrorResponse'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/models.ErrorResponse'
      summary: Get biopsias by date range
      tags:
      - biopsias
  /biopsias/performance:
    post:
      consumes:
      - application/json
      description: Insert multiple biopsias with the same data for performance testing
      parameters:
      - description: Biopsia to create
        in: body
        name: biopsia
        required: true
        schema:
          $ref: '#/definitions/models.BiopsiaSwagger'
      - description: Number of times to insert the biopsia
        in: query
        name: count
        required: true
        type: integer
      produces:
      - application/json
      responses:
        "201":
          description: Created
          schema:
            $ref: '#/definitions/models.BiopsiaSwagger'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/models.ErrorResponse'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/models.ErrorResponse'
      summary: Insert multiple biopsias for performance testing
      tags:
      - biopsias
  /biopsias/search:
    get:
      consumes:
      - application/json
      description: Get biopsias by a specific condition
      parameters:
      - description: Condition
        in: query
        name: condition
        required: true
        type: string
      - description: Value
        in: query
        name: value
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            items:
              $ref: '#/definitions/models.BiopsiaSwagger'
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/models.ErrorResponse'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/models.ErrorResponse'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/models.ErrorResponse'
      summary: Get biopsias by a specific condition
      tags:
      - biopsias
swagger: "2.0"
