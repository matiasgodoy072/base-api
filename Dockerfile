# Etapa 1: Construcción
FROM golang:1.22-alpine as builder

# Establecer el directorio de trabajo dentro del contenedor
WORKDIR /app

# Instalar swag para generar la documentación
RUN go install github.com/swaggo/swag/cmd/swag@latest

# Copiar los archivos go.mod y go.sum para descargar las dependencias primero
COPY go.mod go.sum ./

# Instalar las dependencias del proyecto
RUN go mod download

# Copiar el resto de los archivos del proyecto al directorio de trabajo del contenedor
COPY . .

# Ejecutar swag init para generar la documentación
RUN swag init

# Construir la aplicación
RUN go build -o main .

# Etapa 2: Ejecución
FROM alpine:3.14

# Establecer el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copiar el binario construido desde la etapa anterior
COPY --from=builder /app/main .
COPY --from=builder /app/docs ./docs

# Exponer el puerto en el que la aplicación correrá
EXPOSE 8080

# Definir variables de entorno
ENV PORT=8080

# Comando para correr la aplicación
CMD ["./main"]
