package main

import (
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "go-biopsia/docs"
	"go-biopsia/internal/config"
	"go-biopsia/internal/repositories"
	"go-biopsia/internal/routes"
	"log"
	"os"
	"strings"
	"time"
)

// @title My Project API
// @version 1.0
// @description This is a sample server for a biopsia management application.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

func main() {
	// Crear instancia de Gin
	r := gin.Default()

	// Obtener el puerto del entorno
	port := os.Getenv("PORT")

	//configuración de swagger
	swagger_host := os.Getenv("SWAGGER_HOST")
	if port == "" {
		port = "8080" // Valor por defecto si PORT no está definido
	}
	configureSwagger(swagger_host)

	// Configuración de la base de datos
	db := config.InitDB()

	// Obtener la conexión SQL subyacente
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalf("Error al obtener la conexión SQL: %v", err)
	}
	defer sqlDB.Close() // Cerrar la conexión al finalizar

	// Configurar el middleware CORS
	configCors := cors.DefaultConfig()
	configCors.AllowAllOrigins = true
	configCors.AllowMethods = []string{"POST", "GET", "PUT", "OPTIONS"}
	configCors.AllowHeaders = []string{"Origin", "Content-Type", "Authorization", "Accept", "User-Agent", "Cache-Control", "Pragma"}
	configCors.ExposeHeaders = []string{"Content-Length"}
	configCors.AllowCredentials = true
	configCors.MaxAge = 12 * time.Hour

	r.Use(cors.New(configCors))
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.Run(fmt.Sprintf(":%s", port)) // por defecto escucha en :8080

	//inicializar el repositorios
	biopsiaRepo := repositories.NewBiopsiaRepository(db)

	// Configurar rutas
	routes.BiopsiaRoutes(r, biopsiaRepo)

}

func configureSwagger(swagger_host string) {
	doc := "docs/docs.go" // Path to your generated docs.go
	input, err := os.ReadFile(doc)
	if err != nil {
		log.Fatalf("Error reading swagger doc: %v", err)
	}

	output := os.ExpandEnv(string(input))
	output = replaceHost(output, swagger_host)

	err = os.WriteFile(doc, []byte(output), 0644)
	if err != nil {
		log.Fatalf("Error writing swagger doc: %v", err)
	}
}

func replaceHost(input, swagger_host string) string {
	oldHost := "@host localhost:8080"
	newHost := "@host " + swagger_host
	return strings.Replace(input, oldHost, newHost, 1)
}
